import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import terser from "@rollup/plugin-terser";
import scss from "rollup-plugin-scss";
import postcss from "postcss";
import autoprefixer from "autoprefixer";
import dts from "rollup-plugin-dts";
import json from "@rollup/plugin-json";
import external from "rollup-plugin-peer-deps-external";
import packageJson from "./package.json" assert { type: "json" };

export default [
  {
    input: "src/index.ts",
    output: [
      {
        file: packageJson.main,
        format: "cjs",
        sourcemap: true,
      },
      {
        file: packageJson.module,
        format: "esm",
        sourcemap: true,
        plugins: [terser()],
      },
    ],
    plugins: [
      resolve({ extensions: [".ts", ".tsx"] }),
      commonjs(),
      json(),
      external(),
      typescript({
        tsconfig: "./tsconfig.json",
      }),

      scss({
        processor: () => postcss([autoprefixer()]),
        failOnError: true,
        outputStyle: "compressed",
      }),
    ],
  },
  {
    input: "dist/esm/types/index.d.ts",
    output: [{ file: "dist/index.d.ts", format: "esm" }],
    plugins: [dts(), resolve({ extensions: [".ts", ".tsx"] })],
  },
];
